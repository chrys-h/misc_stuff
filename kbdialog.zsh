#!/usr/bin/env zsh

# See http://blog.etoilebsd.net/post/Zsh_avec_Ncurses_c_est_beau

zmodload zsh/curses

# Ugly hack
lf="
"

typeset -A entries
entries[0]="French"
entries[1]="Russian"
entries[2]="Devanagari"
entries[3]="Greek"
curr=0
term_lines=0
term_cols=0

pow_dlg()
{
	pow_dlg.draw()
	{
		zcurses addwin main $(( $LINES - 2 )) $(( $COLUMNS - 4 )) 1 2
		zcurses attr main +bold
		label="Select keyboard layout:"
		zcurses move main 0 0
		zcurses string main $label
		for entry ({0..3}); do
			txt=${entries[$entry]}
			pos_x=$(( ($COLUMNS - 4 - $#txt) / 2 ))
			pos_y=$(( (($LINES - 4) / 2) + $entry))
			if (( $entry == $curr )); then
				zcurses attr main default/default +reverse
			else
				zcurses attr main default/default -reverse
			fi
			for curr_x ({0..$pos_x}); do
				zcurses move main $pos_y $curr_x
				zcurses char main ' '
			done
			zcurses string main $txt
			for curr_x ({$(( $pos_x + $#txt + 1 ))..$(($COLUMNS - 5))}); do
				zcurses move main $pos_y $curr_x
				zcurses char main ' '
			done
		done

		# Ugly hack to hide the cursor
		zcurses attr main default/default +reverse
		zcurses char main ' '
		zcurses move main $(( $pos_y + 1 )) 0

		zcurses refresh main
	}

	pow_dlg.listen()
	{
		while true; do
			if [[ -z $key ]]; then
				zcurses input main char key
			fi
			newkey=$key
			key=
			case "$char:$newkey" in
				:UP)
					if (( $curr == 0 )); then
						curr=3
					else
						curr=$(( $curr - 1 ))
					fi
					pow_dlg.redraw
					;;

				:DOWN)
					if (( $curr == 3 )); then
						curr=0
					else
						curr=$(( $curr + 1 ))
					fi
					pow_dlg.redraw
					;;
				"f:")
					curr=0
					break
					;;
				"r:")
					curr=1
					break
					;;
				"d:")
					curr=2
					break
					;;
				"g:")
					curr=3
					break
					;;
				" :"|"$lf:")
					break
					;;
			esac
		done
	}

	pow_dlg.redraw()
	{
		for win ($zcurses_windows); do
			zcurses delwin $win 2>/dev/null
		done
		pow_dlg.draw
	}

	# 'Constructor'
	{
		zcurses init
		term_lines=$LINES
		term_cols=$COLUMNS
		pow_dlg.draw
		pow_dlg.listen
	}

	always
	{
		for win ($zcurses_windows); do
			zcurses delwin $win 2>/dev/null
		done
		zcurses delwin main
		zcurses end
	}
}

print_centered()
{
	tput civis
	txt=$1
	skip_lines=$(( $term_lines / 2 ))
	skip_cols=$(( ($term_cols - $#txt) / 2 ))
	for i ({1..$skip_lines}); do
		echo ""
	done
	for i ({1..$skip_cols}); do
		echo -n " "
	done
	echo "$txt"
}

pow_dlg

case "$curr" in
 0)
	setxkbmap fr
	;;

 1)
	setxkbmap ru
	;;

 2)
	setxkbmap dev
	;;

 3)
	setxkbmap gr
	;;
 *)
	print_centered "Invalid \$curr: $curr"
	sleep 5
	;;
esac


