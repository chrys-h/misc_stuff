#/usr/bin/env bash

color=$(cat $HOME/.Xdefaults | grep background | awk '{print $NF}')
# echo background: $color
convert -size 1x1 xc:$color /tmp/pixel.jpg
feh --bg-tile /tmp/pixel.jpg
